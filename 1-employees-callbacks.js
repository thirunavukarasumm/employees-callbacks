/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
*/



const fs = require("fs");
const path = require("path");


function retriveData(id) {
    fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
        if (err) {
            throw new Error(err);

        } else {
            let resultJSON = JSON.parse(data)["employees"].filter((data) => {
                return id.includes(data.id)
            }).reduce((accu, currentData) => {
                accu["employees"].push(currentData);
                return accu;
            }, { employees: [] })
            fs.writeFile(path.join(__dirname, "retrieveData.json"), JSON.stringify(resultJSON), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })
        }
    })
}

// retriveData([2, 13, 23])


function groupDataBasedOnCompany(callback) {
    fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
        if (err) {
            throw new Error(err);
        } else {
            let resultJSON = JSON.parse(data)["employees"].reduce((accu, currentData) => {
                if (accu[currentData.company]) {
                    accu[currentData.company].push(currentData);
                } else {
                    accu[currentData.company] = [currentData];
                }
                return accu;
            }, {})
            fs.writeFile(path.join(__dirname, "groupData.json"), JSON.stringify(resultJSON), (err) => {
                if (err) {
                    throw new Error(err);
                    callback(err);
                } else {
                    if (typeof callback === "function") {
                        callback(null, resultJSON);
                    }

                }
            })
        }
    })
}

// groupDataBasedOnCompany()

function getDataOfACompany(company) {
    groupDataBasedOnCompany((err, data) => {
        if (err) {
            throw new Error(err);
        } else {
            fs.writeFile(path.join(__dirname, "getDataOfACompany.json"), JSON.stringify(data[company]), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })
        }
    })
}


// getDataOfACompany("data.json","Scooby Doo")


function removeID(id) {
    fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
        if (err) {
            throw new Error(err);

        } else {
            let resultJSON = JSON.parse(data)["employees"].filter((data) => {
                return data["id"] !== id;
            })
            fs.writeFile(path.join(__dirname, "removeID.json"), JSON.stringify(resultJSON), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })
        }
    })
}

// removeID(2)


function sortCompany() {
    fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
        if (err) {
            throw new Error(err);

        } else {
            let resultJSON = JSON.parse(data)["employees"].sort((companyA, companyB) => {
                if (companyA.company !== companyB.company) {
                    return companyA.company.localeCompare(companyB.company)
                } else {
                    return companyA.id - companyB.id;
                }

            })
            fs.writeFile(path.join(__dirname, "sortedCompany.json"), JSON.stringify(resultJSON), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })

        }
    })
}

// sortCompany()


function swapCompany(swap1, swap2) {
    fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
        if (err) {
            throw new Error(err);

        } else {
            let resultJSON = JSON.parse(data)["employees"];
            let swap1data;
            let swap2data;
            for (let index = 0; index < resultJSON.length; index++) {
                if (resultJSON[index]["id"] === swap1) {
                    swap1 = index
                    swap1data = resultJSON[index];
                } else if (resultJSON[index]["id"] === swap2) {
                    swap2 = index
                    swap2data = resultJSON[index];
                }

            }
            resultJSON.splice(swap1, swap1, swap2data);
            resultJSON.splice(swap2, swap2, swap1data);
            fs.writeFile(path.join(__dirname, "swapCompany.json"), JSON.stringify(resultJSON), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })

        }
    })
}
swapCompany(93, 92)